<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Appointment test suites</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>00186c42-c229-42e5-b40b-12eb4bb7b02b</testSuiteGuid>
   <testCaseLink>
      <guid>96133c9d-6616-42b7-b9cf-b56dca0e2b68</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e3227dd-d166-49ef-92a0-d65e3bf3d292</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Fail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d1f40b8a-33cf-4553-831e-d3fb8c8b1eb5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Make Appointment</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
